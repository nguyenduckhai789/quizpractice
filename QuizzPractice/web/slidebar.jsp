<%-- 
    Document   : slidebar
    Created on : Nov 30, 2022, 10:37:43 PM
    Author     : Thanh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>A</title>
        <link rel="shortcut icon" href="assets/img/favicon.png">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,500;0,600;0,700;1,400&amp;display=swap">

        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">

        <link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
        <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">

        <link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css">

        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="menu-title">
                            <span>Main Menu</span>
                        </li>
                        <li class="submenu">
                            <a href="#"><i class="fas fa-user-graduate"></i> <span> Category</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="index.html">List Category</a></li>
                                <li><a href="teacher-dashboard.html">Add Category</a></li>
                            </ul>
                        </li>
                        <li class="submenu">
                            <a href="#"><i class="fas fa-user-graduate"></i> <span> Subject</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="students.html">List Subject</a></li>
                                <li><a href="student-details.html">Add Subject</a></li>
                            </ul>
                        </li>
                        <li class="submenu">
                            <a href="#"><i class="fas fa-chalkboard-teacher"></i> <span> Class</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="teachers.html">List Class</a></li>
                                <li><a href="teacher-details.html">Add Class</a></li>
                            </ul>
                        </li>
                        <li class="submenu">
                            <a href="#"><i class="fas fa-building"></i> <span> Question</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="ListIterationController">List Question</a></li>
                                <li><a href="IterationDetailsController">Add Question</a></li>
                            </ul>
                        </li>
                       
                        <li class="menu-title">
                            <span>Management</span>
                        </li>
                        <li class="submenu">
                            <a href="#"><i class="fas fa-file-invoice-dollar"></i> <span> Accounts</span> <span class="menu-arrow"></span></a>
                            <ul>
                                <li><a href="expenses.html">List Account</a></li>
                                <li><a href="salary.html">Add Account</a></li>
                                <li><a href="salary.html">Setting</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="index.html"><i class="fas fa-clipboard-list"></i> <span>Issues</span></a>
                        </li>
                        
                       
                    </ul>
                </div>
            </div>
        </div>
    </body>
    <script src="assets/js/jquery-3.6.0.min.js"></script>

    <script src="assets/js/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/datatables.min.js"></script>

    <script src="assets/js/script.js"></script>
</html>

