/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Category;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.jboss.logging.LogMessage;
/**
 *
 * @author duong
 */
public class CategoryDAO extends DBContext implements Serializable{
    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    private void closeConnection() throws Exception {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }
    
    public ArrayList<Category> getAllCategories() throws SQLException, Exception {
        ArrayList<Category> listCategory = new ArrayList<>();
        String sql = "SELECT Id, Name, Status, CreateAt, LastModifyAt FROM Categories";
        try {
            conn = getConnection();
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCategoryId(rs.getInt("Id"));
                category.setCategoryName(rs.getString("Name"));
                category.setStatus(rs.getBoolean("Status"));
                category.setCreateAt(rs.getString("CreateAt"));
                category.setLastModifyAt(rs.getString("LastModifyAt"));
                listCategory.add(category);
            }
        } catch (Exception e) {
        } finally {
            closeConnection();
        }
        return listCategory;
    }
}