/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Define;

/**
 *
 * @author
 */
public class Define {
    public static final String INDEX_PAGE = "index.html";
    public static final String CATEGORY_PAGE = "category.jsp";
    public static final String SUBJECT_PAGE = "subject.jsp";
    
    public static final String MAIN_CONTROLLER = "MainController";
    public static final String CATEGORY_CONTROLLER = "CategoryController";
    public static final String SUBJECT_CONTROLLER = "SubjectController";
}
