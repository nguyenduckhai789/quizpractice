import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 
 */
public class CategoryDAO implements Serializable{
    private Connection conn;
    private PreparedStatement preStm;
    private ResultSet rs;

    private void closeConnection() throws Exception {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }
    }
    
    public ArrayList<CategoryDTO> getAllCategories() throws SQLException, Exception {
        ArrayList<CategoryDTO> listCategory = new ArrayList<>();
        String sql = "SELECT Id, Name, Status, CreateAt, LastModifyAt FROM Categories";
        try {
            conn = DBUitils.implementConnection();
            preStm = conn.prepareStatement(sql);
            rs = preStm.executeQuery();
            while (rs.next()) {
                CategoryDTO category = new CategoryDTO();
                category.setCategoryId(rs.getInt("Id"));
                category.setCategoryName(rs.getString("Name"));
                category.setStatus(rs.getInt("Status"));
                category.setCreateAt(rs.getString("CreateAt"));
                category.setLastModifyAt(rs.getString("LastModifyAt"));
                listCategory.add(category);
            }
        } catch (Exception e) {
        } finally {
            closeConnection();
        }
        return listCategory;
    }
}
