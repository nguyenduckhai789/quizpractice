
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 
 */
public class SubjectDTO implements Serializable{
    private int subjectId;
    private String subjectName;
    private String description;
    private int categoryId;
    private String createAt;

    public SubjectDTO() {
    }

    public SubjectDTO(int subjectId, String subjectName, String description, int categoryId, String createAt) {
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.description = description;
        this.categoryId = categoryId;
        this.createAt = createAt;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    
}
