
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 
 */
public class CategoryDTO implements Serializable{
    private int categoryId;
    private String categoryName;
    private Boolean status;
    private String createAt;
    private String lastModifyAt;

    public CategoryDTO() {
    }

    public CategoryDTO(int categoryId, String categoryName, Boolean status, String createAt, String lastModifyAt) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.status = status;
        this.createAt = createAt;
        this.lastModifyAt = lastModifyAt;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getLastModifyAt() {
        return lastModifyAt;
    }

    public void setLastModifyAt(String lastModifyAt) {
        this.lastModifyAt = lastModifyAt;
    }
    
}
