USE [SPW391_Block5]
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [Name], [Status], [CreateAt], [LastModifyAt]) VALUES (2, N'Software Engineering', 1, CAST(N'2022-11-30T00:00:00.0000000' AS DateTime2), CAST(N'2022-11-30T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Categories] ([Id], [Name], [Status], [CreateAt], [LastModifyAt]) VALUES (4, N'Graphic Design', 1, CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2), CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Categories] ([Id], [Name], [Status], [CreateAt], [LastModifyAt]) VALUES (6, N'Business Administration', 1, CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2), CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Categories] ([Id], [Name], [Status], [CreateAt], [LastModifyAt]) VALUES (7, N'Fundamental', 1, CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2), CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Categories] ([Id], [Name], [Status], [CreateAt], [LastModifyAt]) VALUES (8, N'Profession Certification', 1, CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2), CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Categories] ([Id], [Name], [Status], [CreateAt], [LastModifyAt]) VALUES (9, N'Multimedia Communications', 1, CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2), CAST(N'2022-12-01T00:00:00.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[Categories] OFF
